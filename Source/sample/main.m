#import <objc/objc.h>
#import <objc/Object.h>
#import <Foundation/Foundation.h>

#define MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER 4
#define MAXIMUM_ALLOWED_ITEMS_IN_CHIPS_FRIER 4
#define MAXIMUM_WAITING_TIME_FOR_ANY_ITEM_IN_SECONDS 120
#define MAXIMUM_PREPARATION_TIME_FOR_ANY_ORDER 600

typedef enum OrderState {
    arrivedIn,
    accepted,
    rejected,
    processing,
    served
} OrderState;

extern NSString * const OrderState_toString[];

NSString * const OrderState_toString[] = {
    [arrivedIn] = @"Arrived In",
    [accepted] = @"Accepted",
    [rejected] = @"Rejected",
    [processing] = @"Processing",
    [served] = @"Served"
};

typedef enum ItemType {
    batteredHaddock,
    batteredCod,
    chips,
} ItemType;

extern NSString * const ItemType_toString[];

NSString * const ItemType_toString[] = {
    [batteredHaddock] = @"Haddock",
    [batteredCod] = @"Cod",
    [chips] = @"Chips"
};

typedef enum ComparisonResult {
    equal,
    greater,
    lesser,
} ComparisonResult;

@interface Time :NSObject

@property (nonatomic,assign) NSInteger hours;
@property (nonatomic,assign) NSInteger minutes;
@property (nonatomic,assign) NSInteger seconds;

/**
 *  Custom initialization method used to get instace of Time
 *
 *  @param time an instance of Time passed as a argument to initialize the values.
 *
 *  @return An instance of Time
 */
-(instancetype)initWithTime:(Time *)time;

/**
 *  Custom initialization method used to get instace of Time
 *
 *  @param seconds Argument passed to calculate and initialize values of data members
 *
 *  @return An instance of Time
 */
-(instancetype)initWithSeconds:(NSInteger)seconds;

/**
 *  Custom initialization method used to get instace of Time
 *
 *  @param string Argument passed to calculate and initialize values of data members
 *
 *  @return An instance of Time
 */
-(instancetype)initWithString:(NSString *)string;

/**
 *  Instance method used to add time object's values to the existing object's data members
 *
 *  @param timeToBeAdded Argument passed to calculate and initialize values of data members
 *
 *  @return An instance of Time
 */
-(Time *)addTime:(Time *)timeToBeAdded;

/**
 *  Instance method used to get the values existing time object in seconds <NSInteger>
 *
 *  @return value of existing time into seconds <NSInteger>
 */
-(NSInteger)inSeconds;

/**
 *  Instance method used to get the formatted string ( HH:MM:SS ) of existing time object
 *
 *  @return formatted string ( HH:MM:SS ) of existing time object
 */
-(NSString *)formattedString;

/**
 *  Instance method used to compare whether the existing time object is greater , equal or lesser than the argument
 *
 *  @param time Argument with whom the existing time is to be compared
 *
 *  @return Comparison result < greater , eqaul , lesser >
 */
-(ComparisonResult)compareWith:(Time *)time;

@end

@implementation Time
-(instancetype)initWithTime:(Time *)time{
    if ((self = [super init])){
        self.hours = time.hours;
        self.minutes = time.minutes;
        self.seconds = time.seconds;
    }
    return self;
}

-(instancetype)initWithSeconds:(NSInteger)seconds{
    if ((self = [super init])){
        self.seconds = seconds % 60;
        self.minutes = seconds / 60;
        self.hours = seconds / (60*60);
    }
    return self;
}

-(instancetype)initWithString:(NSString *)string{
    if ((self = [super init])){
        NSArray *timeComponents = [[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] componentsSeparatedByString:@":"];
        self.seconds = [[timeComponents objectAtIndex:2] integerValue]%60;
        self.minutes = ([[timeComponents objectAtIndex:1] integerValue] + [[timeComponents objectAtIndex:2] integerValue]/60)%60;
        self.hours = ([[timeComponents objectAtIndex:0] integerValue] + [[timeComponents objectAtIndex:1] integerValue]/60)%24;
    }
    return self;
}

-(Time *)addTime:(Time *)timeToBeAdded{
    if (timeToBeAdded) {
        long hoursToBeAdded = timeToBeAdded.hours;
        long minutesToBeAdded = timeToBeAdded.minutes;
        long secondsToBeAdded = timeToBeAdded.seconds;
        
        self.hours = (self.hours + hoursToBeAdded + (self.minutes + minutesToBeAdded)/60 + (self.seconds + secondsToBeAdded)/(60*60))%24;
        self.minutes = (self.minutes + minutesToBeAdded + (self.seconds + secondsToBeAdded)/60)%60;
        self.seconds = (self.seconds + secondsToBeAdded) % 60;
    }
    return self;
}

-(NSString *)formattedString{
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",self.hours,self.minutes,self.seconds];
}

-(NSInteger)inSeconds{
    return (self.seconds + self.minutes*60 + self.hours*60*60);
}

-(ComparisonResult)compareWith:(Time *)time{
    if (self.hours == time.hours && self.minutes == time.minutes && self.seconds == time.seconds){
        return equal;
    }
    else{
        if ([self inSeconds] > [time inSeconds]){
            return greater;
        }
        else{
            return lesser;
        }
    }
}

@end

@interface Item : NSObject

@property(nonatomic,assign) NSInteger count;
@property(nonatomic,assign) ItemType type;
@property(nonatomic,strong) Time *cookingTime;

/**
 *  Custom initialization method used to get the instance of Item
 *
 *  @param itemCount count of number of items with whom the (Item *) is to be initialized
 *
 *  @return An instance of Item
 */
-(instancetype)initWithCount:(NSInteger)itemCount;

@end

@implementation Item
-(instancetype)initWithCount:(NSInteger)itemCount{
    if ((self = [super init])){
        self.count = itemCount;
    }
    return self;
}

@end

@interface BatteredCod : Item
@end

@implementation BatteredCod

/**
 *  Overrided method of base class (Item) , used to get the instance of BatteredCod
 *
 *  @param itemCount itemCount count of number of items with whom the (BatteredCod *) is to be initialized
 *
 *  @return An instance of BatteredCod
 */
-(instancetype)initWithCount:(NSInteger)itemCount{
    if ((self = [super initWithCount:itemCount])){
        self.type = batteredCod;
        self.cookingTime = [[Time alloc] initWithSeconds:80];
    }
    return self;
}

@end

@interface BatteredHaddock : Item
@end

@implementation BatteredHaddock

/**
 *  Overrided method of base class (Item) , used to get the instance of BatteredHaddock
 *
 *  @param itemCount itemCount count of number of items with whom the (BatteredHaddock *) is to be initialized
 *
 *  @return An instance of BatteredHaddock
 */
-(instancetype)initWithCount:(NSInteger)itemCount{
    if ((self = [super initWithCount:itemCount])){
        self.type = batteredHaddock;
        self.cookingTime = [[Time alloc] initWithSeconds:90];
    }
    return self;
}

@end

@interface Chips : Item
@end

@implementation Chips

/**
 *  Overrided method of base class (Item) , used to get the instance of Chips
 *
 *  @param itemCount itemCount count of number of items with whom the (Chips *) is to be initialized
 *
 *  @return An instance of Chips
 */
-(instancetype)initWithCount:(NSInteger)itemCount{
    if ((self = [super initWithCount:itemCount])){
        self.type = chips;
        self.cookingTime = [[Time alloc] initWithSeconds:120];
    }
    return self;
}

@end

@interface Utility :NSObject

/**
 *  A utility method used to get a instance of Item based upon the parametric string
 *
 *  @param string argument used to differentiate and generate the relevant intance of Item
 *
 *  @return An instance of Item ( BatteredCod *, BatteredHaddock *, Chips * )
 */
+(Item *)itemUsingString:(NSString *)string;

@end

@implementation Utility
+(Item *)itemUsingString:(NSString *)string{
    NSArray *components = [string componentsSeparatedByString:@" "];
    NSInteger indexPositionForItemCount = 1;
    if ([components.lastObject isEqualToString:ItemType_toString[batteredCod]]){
        return [[BatteredCod alloc] initWithCount:[[components objectAtIndex:indexPositionForItemCount] integerValue]];
    }
    else if ([components.lastObject isEqualToString:ItemType_toString[batteredHaddock]]){
        return [[BatteredHaddock alloc] initWithCount:[[components objectAtIndex:indexPositionForItemCount] integerValue]];
    }
    else{
        return [[Chips alloc] initWithCount:[[components objectAtIndex:indexPositionForItemCount] integerValue]];
    }
}

@end

@interface Order : NSObject

@property (nonatomic,assign) OrderState state;
@property (nonatomic,assign) NSInteger sequence;
@property (nonatomic,strong) Time *arrivedInTime;
@property (nonatomic,strong) Time *servedAtTime;
@property (nonatomic,strong) BatteredCod *batteredCod;
@property (nonatomic,strong) BatteredHaddock *batteredHaddock;
@property (nonatomic,strong) Chips *chips;

/**
 *  Custom initialization method used to get the instance of Order
 *
 *  @param orderString agrument containing various information related to order to be generated
 *
 *  @return An instance of Order
 */
-(instancetype)initWithString:(NSString *)orderString;

@end

@implementation Order : NSObject
-(instancetype)initWithString:(NSString *)orderString{
    if (self = [super init]){
        NSArray *components = [orderString componentsSeparatedByString:@","];
        for (int index = 2;index < components.count; index++){
            NSString *orderString = [components objectAtIndex:index];
            Item *item = (Item *)[Utility itemUsingString:orderString];
            switch (item.type) {
                case batteredCod:
                    self.batteredCod = (BatteredCod *)item;
                    break;
                case batteredHaddock:
                    self.batteredHaddock = (BatteredHaddock *)item;
                    break;
                case chips:
                    self.chips = (Chips *)item;
                    break;
            }
        }
        
        self.arrivedInTime = [[Time alloc] initWithString:[components objectAtIndex:1]];
        NSString *sequenceString = components.firstObject;
        NSArray *sequenceComponents = [sequenceString componentsSeparatedByString:@"#"];
        self.sequence = [sequenceComponents.lastObject integerValue];
        
        self.state = arrivedIn;
    }
    return self;
}

@end

static NSString * readLine() {
    char buffer[512];
    
    if (fgets(buffer, sizeof(buffer), stdin) != NULL) {
        return [[NSString stringWithUTF8String:buffer] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    
    return nil;
}

int main ( int argc, const char *argv[] ) {
    
    @autoreleasepool
    {
        NSString * line = readLine();
        
        Time *previousOrderServedAt;
        Time *machineTime;
        
        while (line){
            Order *order = [[Order alloc] initWithString:line];
            
            long batteredCodCount = order.batteredCod.count;
            long batteredHaddockCount = order.batteredHaddock.count;
            long chipsCount = order.chips.count;
            
            if (machineTime ==nil){
                machineTime = order.arrivedInTime;
            }
            else{
                machineTime = nil;
                
                switch ([order.arrivedInTime compareWith:previousOrderServedAt]) {
                    case equal:
                        machineTime = order.arrivedInTime;
                        break;
                    case greater:
                        machineTime = order.arrivedInTime;
                        break;
                    case lesser:
                        machineTime = previousOrderServedAt;
                        break;
                }
            }
            
            while (order.state != served || order.state == rejected) {
                long cookingTimeOfCod = (batteredCodCount/MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER)*[order.batteredCod.cookingTime inSeconds];
                long cookingTimeOfHaddock = (batteredHaddockCount/MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER)*[order.batteredHaddock.cookingTime inSeconds];
                
                if (((batteredHaddockCount%MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER)+(batteredCodCount%MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER)) > MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER){
                    cookingTimeOfHaddock += [order.batteredHaddock.cookingTime inSeconds];
                    cookingTimeOfCod += [order.batteredCod.cookingTime inSeconds];
                }
                else{
                    if (batteredHaddockCount%MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER == 0){
                        if ((batteredCodCount%MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER) != 0) {
                            cookingTimeOfCod += [order.batteredCod.cookingTime inSeconds];
                        }
                    }
                    else{
                        cookingTimeOfHaddock += [order.batteredHaddock.cookingTime inSeconds];
                    }
                }
                
                long combinedCookingTimeOfCodAndHaddock = cookingTimeOfCod + cookingTimeOfHaddock;
                long cookingTimeOfChips = (chipsCount/MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER)*[order.chips.cookingTime inSeconds];
                
                if ((chipsCount%MAXIMUM_ALLOWED_ITEMS_IN_CHIPS_FRIER) != 0){
                    cookingTimeOfChips += [order.chips.cookingTime inSeconds];
                }
                
                if (order.state == arrivedIn){
                    if (((chipsCount/MAXIMUM_ALLOWED_ITEMS_IN_CHIPS_FRIER)-1)*[order.chips.cookingTime inSeconds] > MAXIMUM_WAITING_TIME_FOR_ANY_ITEM_IN_SECONDS){
                        order.state = rejected;
                        printf("at %s, Order #%ld Rejected\n",[[order.arrivedInTime formattedString] UTF8String],order.sequence);
                        break;
                    }
                    else if (((batteredCodCount/MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER)-1)*[order.batteredCod.cookingTime inSeconds] > MAXIMUM_WAITING_TIME_FOR_ANY_ITEM_IN_SECONDS){
                        order.state = rejected;
                        printf("at %s, Order #%ld Rejected\n",[[order.arrivedInTime formattedString] UTF8String],order.sequence);
                        break;
                    }
                    else if (((batteredHaddockCount/MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER)-1)*[order.batteredHaddock.cookingTime inSeconds] > MAXIMUM_WAITING_TIME_FOR_ANY_ITEM_IN_SECONDS){
                        order.state = rejected;
                        printf("at %s, Order #%ld Rejected\n",[[order.arrivedInTime formattedString] UTF8String],order.sequence);
                        break;
                    }
                    else if (combinedCookingTimeOfCodAndHaddock > cookingTimeOfChips) {
                        if (([machineTime inSeconds] + combinedCookingTimeOfCodAndHaddock) > ([order.arrivedInTime inSeconds]+MAXIMUM_PREPARATION_TIME_FOR_ANY_ORDER))
                        {
                            order.state = rejected;
                            printf("at %s, Order #%ld Rejected\n",[[order.arrivedInTime formattedString] UTF8String],order.sequence);
                            break;
                        }
                        else{
                            order.state = processing;
                            printf("at %s, Order #%ld Accepted\n",[[order.arrivedInTime formattedString] UTF8String],order.sequence);
                        }
                    }
                    else{
                        if (([machineTime inSeconds] + cookingTimeOfChips) > ([order.arrivedInTime inSeconds]+MAXIMUM_PREPARATION_TIME_FOR_ANY_ORDER))
                        {
                            order.state = rejected;
                            printf("at %s, Order #%ld Rejected\n",[[order.arrivedInTime formattedString] UTF8String],order.sequence);
                            break;
                        }
                        else{
                            order.state = processing;
                            printf("at %s, Order #%ld Accepted\n",[[order.arrivedInTime formattedString] UTF8String],order.sequence);
                        }
                    }
                }
                
                if (combinedCookingTimeOfCodAndHaddock > cookingTimeOfChips){
                    if (cookingTimeOfHaddock > cookingTimeOfCod){
                        printf("at %s, Begin Cooking %ld Haddock\n",[[machineTime formattedString] UTF8String],((batteredHaddockCount / MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER) == 0)?batteredHaddockCount:MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER);
                        if (cookingTimeOfChips == 0) {
                            combinedCookingTimeOfCodAndHaddock = [order.batteredHaddock.cookingTime inSeconds];
                        }
                        machineTime = [machineTime addTime:[[Time alloc] initWithSeconds:combinedCookingTimeOfCodAndHaddock-cookingTimeOfChips]];
                        
                        if ((batteredHaddockCount / MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER) == 0){
                            batteredHaddockCount = 0;
                        }
                        else{
                            batteredHaddockCount -= MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER;
                        }
                    }
                    else{
                        printf("at %s, Begin Cooking %ld Cod\n",[[machineTime formattedString] UTF8String],((batteredCodCount / MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER) == 0)?batteredCodCount:MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER);
                        if (cookingTimeOfChips == 0) {
                            combinedCookingTimeOfCodAndHaddock = [order.batteredCod.cookingTime inSeconds];
                        }
                        machineTime = [machineTime addTime:[[Time alloc] initWithSeconds:combinedCookingTimeOfCodAndHaddock-cookingTimeOfChips]];
                        
                        if ((batteredCodCount / MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER) == 0){
                            batteredCodCount = 0;
                        }
                        else{
                            batteredCodCount -= MAXIMUM_ALLOWED_ITEMS_IN_FISH_FRIER;
                        }
                    }
                }
                else{
                    printf("at %s, Begin Cooking %ld Chips\n",[[machineTime formattedString] UTF8String],((chipsCount / MAXIMUM_ALLOWED_ITEMS_IN_CHIPS_FRIER) == 0)?chipsCount:MAXIMUM_ALLOWED_ITEMS_IN_CHIPS_FRIER);
                    if (combinedCookingTimeOfCodAndHaddock == 0) {
                        cookingTimeOfChips = [order.chips.cookingTime inSeconds];
                    }
                    machineTime = [machineTime addTime:[[Time alloc] initWithSeconds:cookingTimeOfChips-combinedCookingTimeOfCodAndHaddock]];
                    
                    if ((chipsCount / MAXIMUM_ALLOWED_ITEMS_IN_CHIPS_FRIER) == 0){
                        chipsCount = 0;
                    }
                    else{
                        chipsCount -= MAXIMUM_ALLOWED_ITEMS_IN_CHIPS_FRIER;
                    }
                }
                
                if (chipsCount ==0 && batteredCodCount == 0 && batteredHaddockCount == 0){
                    order.state = served;
                    order.servedAtTime = machineTime;
                    
                    if (previousOrderServedAt == nil){
                        previousOrderServedAt = [[Time alloc] initWithTime:order.servedAtTime];
                    }
                    else{
                        previousOrderServedAt = order.servedAtTime;
                    }
                    
                    printf("at %s, Serve Order #%ld\n",[[order.servedAtTime formattedString] UTF8String],order.sequence);
                }
            }
            order = nil;
            line = readLine();
        }
    }
    
    return 0;
}

